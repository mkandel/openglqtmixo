#pragma once
#ifndef _dviSLMDevice
#define _dviSLMDevice

#include <windows.h>
#include <Mmsystem.h>
#include <GL/glew.h>
#include <GL/wglew.h>
#include <GL/gl.h>
//
#include <vector>
#include <mutex>
#include <atomic>
#include <condition_variable>
#include <iostream>
#include <thread>
#include "spinningBarrier.h"
#include  <mutex>
//
#include <QtGui\qimage.h>

using namespace std;

class dviSlmDevice 
{
public:
	  dviSlmDevice(int monitorIn );
	 ~dviSlmDevice(void);
     void setFrame(int framenum);
	 void setFrameToMonochrome(int frame,unsigned char value);
	 void load8bittiff(int frame, const unsigned char* data, int bytes);

private:
	mutex usingTexture;
	void close();
	int Init();
	void createWindow();
	bool hasValidGlContext;
	DWORD m_idThread;
	int g_windowWidth,g_windowHeight;
	void InitGL();
	void renderFrame();
	static	LRESULT CALLBACK WindowProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);
	WNDCLASSEX wcl;
	HWND      g_hWnd;
	HINSTANCE g_hInstance;
	HDC       g_hDC;
	HGLRC     g_hRC;
	HWND hWnd;
	MONITORINFO monitorInfo;
	//For openGL
	int frame;
	float* vertices;
	float* texCoordinates;
	vector<GLubyte*> textureData;
	GLuint texture;
	GLuint programHandle;
	GLuint vaoHandle;
	GLuint* vboHandles;
	static void fillRandomNoise(GLbyte* noise,int n);
	void initShaders();
	void initTextures();
	int windowLoop();
	int glLoop();
	int monitor; 
	//For synchronization
	spinning_barrier* sb;
	bool ping, pong;
	mutex* m;
	condition_variable* cv;
	bool done;
	thread* windowThread;
	thread* openglThread;
};

#endif
