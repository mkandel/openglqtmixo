#pragma once
#ifndef _timepriority
#define _timepriority
#include <Windows.h>
#include <Mmsystem.h> // for timeBeginPeroid
class TimePriority
{
public:
	TimePriority()
	{
		auto foo = timeBeginPeriod(p);
		assert(foo==TIMERR_NOERROR);
	}
	~TimePriority()
	{
		timeEndPeriod(p);
	}

private:
	static const int p=1;
};
#endif