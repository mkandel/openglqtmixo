#ifndef QT5NATIVEHANDLE_H
#define QT5NATIVEHANDLE_H

#include <windows.h>
#include <GL/glew.h>
#include <GL/wglew.h>
#include <GL/gl.h>
#include <QtWidgets/QMainWindow>
#include "ui_qt5nativehandle.h"
#include <QTimer> 
#include <QDebug>
//std stuff
#include <vector>
#include <atomic>
#include <thread>

using namespace std;
class qt5nativehandle : public QMainWindow
{
	Q_OBJECT

	HDC g_hDC;
	HWND g_hWnd;
	HGLRC     g_hRC;
	GLuint texture;
	GLuint programHandle;
	GLuint vaoHandle;
	GLuint* vboHandles;
	int g_windowWidth,g_windowHeight;
	void initShaders();
	void initTextures();
	void renderFrame();
	void setFrameToMonochrome(int frame,unsigned char value);
	int frame;
	std::vector<GLubyte*> textureData;
	void animate();
	atomic<bool> done;
	std::thread* t;
public:
	qt5nativehandle(QWidget *parent = 0);
	void startTheGL();
	~qt5nativehandle();
	void updateColor();
private:
	Ui::qt5nativehandleClass ui;
};

#endif // QT5NATIVEHANDLE_H
