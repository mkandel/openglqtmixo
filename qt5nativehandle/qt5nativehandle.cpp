#include "qt5nativehandle.h"
#define glE() _glE(__FILE__,__LINE__)
void _glE(const char *file, int line)
{
#ifndef brave
	GLenum err=glGetError();
	if (!err)
	{
		return;
	}
	const char*  errString =(const char*)gluErrorString(err);
	qDebug() <<"Glu Error:"<<file <<":"<<line <<": " << errString;
	exit(20);
#endif
}

#define shaderCheck(sh) _shaderCheck(sh,__FILE__,__LINE__)
void _shaderCheck(GLint sh,const char *file, int line)
{
	GLint status;
	glGetShaderiv( sh, GL_COMPILE_STATUS, &status );
	if (GL_FALSE == status)
	{
		GLint logLen;
		glGetShaderiv( sh, GL_INFO_LOG_LENGTH, &logLen );
		if( logLen > 0 )

		{
			char * log = (char *)malloc(logLen);
			GLsizei written=0;
			glGetShaderInfoLog(sh, logLen, &written, log);
			qDebug() <<"SHDR Error:"<<file <<":"<<line <<": " <<log;
			free(log);
		}
		exit(20);
	}
}

#define programCheck(ph) _programCheck(ph,__FILE__,__LINE__)
void _programCheck(GLint ph,const char *file, int line)
{
	GLint status;
	glGetProgramiv( ph, GL_LINK_STATUS, &status );
	if (GL_FALSE == status)
	{
		GLint logLen;
		glGetProgramiv( ph, GL_INFO_LOG_LENGTH, &logLen );
		if( logLen > 0 )
		{
			char * log = (char *)malloc(logLen);
			GLsizei written=0;
			glGetProgramInfoLog(ph, logLen, &written, log);
			qDebug() <<"PRGM Error:"<<file <<":"<<line <<": " <<log;
			free(log);
		}
		exit(20);
	}
}
qt5nativehandle::qt5nativehandle(QWidget *parent)
	: QMainWindow(parent),t(0)
{
	ui.setupUi(this);
	connect(ui.btnQuit,&QPushButton::clicked, 
		this,&QMainWindow::close);
	t = new std::thread(&qt5nativehandle::animate,this);
}
void qt5nativehandle::startTheGL()
{
	//Try to get a handle
	g_hWnd = (HWND)ui.wgdMessWith->winId();
	 qDebug() << "g_hWnd : " << g_hWnd;
	 g_hDC = GetDC(g_hWnd);
	 qDebug() << "g_hDC : " << g_hDC;
	 //
	int pf = 0;
	// GL Settings
	PIXELFORMATDESCRIPTOR pfd;
	memset(&pfd,0,sizeof(PIXELFORMATDESCRIPTOR));
	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pfd.iPixelType = PFD_TYPE_RGBA;
	pfd.cColorBits = 16;
	pfd.cDepthBits = 8;
	pfd.iLayerType = PFD_MAIN_PLANE;
	OSVERSIONINFO osvi = {0};
	osvi.dwOSVersionInfoSize = sizeof(OSVERSIONINFO);
	GetVersionEx(&osvi);
	pf = ChoosePixelFormat(g_hDC, &pfd);
	SetPixelFormat(g_hDC, pf, &pfd);
	if (osvi.dwMajorVersion > 6 || (osvi.dwMajorVersion == 6 && osvi.dwMinorVersion >= 0))
	{
		pfd.dwFlags |= PFD_SUPPORT_COMPOSITION;
	}
	HGLRC tempContext = wglCreateContext(g_hDC);
	qDebug() << "tempContext : " << tempContext;
	wglMakeCurrent(g_hDC, tempContext);
	auto foo = glewInit();
	if (foo != GLEW_OK)
	{
		qDebug() <<"Oh NO!";
	}
	int attribList[] =
	{
		WGL_CONTEXT_MAJOR_VERSION_ARB, 3,
		WGL_CONTEXT_MINOR_VERSION_ARB, 1,
		WGL_CONTEXT_FLAGS_ARB, WGL_CONTEXT_FORWARD_COMPATIBLE_BIT_ARB,
		0, 0
	};
	//
	g_hRC = wglCreateContextAttribsARB(g_hDC,0, attribList);
	wglMakeCurrent(NULL,NULL);
	wglDeleteContext(tempContext);
	wglMakeCurrent(g_hDC, g_hRC);
	SetWindowText(g_hWnd, L"SLM");
}
void qt5nativehandle::setFrameToMonochrome(int frame,unsigned char value)
{
	int bytes = sizeof(GLubyte)*g_windowWidth*g_windowHeight;
	if (frame >= textureData.size())
	{
		auto p = (GLubyte*) malloc(bytes);
		textureData.push_back(p);
	}
	memset(textureData[frame],value,bytes);
}

void qt5nativehandle::animate()
{
	setFrameToMonochrome(0,0);//some default data to avoid null refernces, etc
	startTheGL();
	frame = 0;
	done = false; 
	glE();
	//
	wglSwapIntervalEXT(1);
	initShaders();
	initTextures();
	ShowWindow(g_hWnd,SW_MAXIMIZE);
	while (!done)
	{
		renderFrame();
		_sleep(100);
		SwapBuffers(g_hDC);
	}
	wglDeleteContext(g_hRC);
	g_hRC = 0;
	ReleaseDC(g_hWnd, g_hDC);
}

void qt5nativehandle::renderFrame()
{
#if 1
	static int tic = -1;
	tic = ++tic %2;
	glClearColor(tic, tic, tic, 0.0f);
#endif
	//glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);
	//
	frame=0;
	glClear(GL_COLOR_BUFFER_BIT);
	glBindTexture(GL_TEXTURE_2D,texture);
	glTexSubImage2D(GL_TEXTURE_2D,0,0,0,g_windowWidth,g_windowHeight,GL_RED,GL_UNSIGNED_BYTE,textureData[frame]);
	glBindVertexArray(vaoHandle);
	glDrawArrays(GL_TRIANGLES,0,8);
	glBindVertexArray(0);
	glBindTexture(GL_TEXTURE_2D,0);
	glFlush();
	glFinish();
	glE();
}

void qt5nativehandle::initShaders()
{
	//Reserve the textures and make the shaders
	const char *vsrc1 =
		"attribute vec2 coord2d;\n"
		"attribute vec4 texCoord;\n"
		"varying vec4 texc;\n"
		"void main()\n"
		"{\n"
		"vec4 vPosition =vec4(coord2d, 0.0, 1.0);\n"
		"gl_Position = vPosition; \n"
		"texc = texCoord;\n"
		"}\n";
	GLint vsH = glCreateShader(GL_VERTEX_SHADER);
	glShaderSource(vsH,1,&vsrc1,NULL);
	glCompileShader(vsH);
	shaderCheck(vsH);
	glE();
	const char *fsrc1 =
		"uniform sampler2D texture;\n"
		"varying vec4 texc;\n"
		"void main(void)\n"
		"{\n"
		"    gl_FragColor = texture2D(texture, texc.st);\n"
		"}\n";
	GLint fsH = glCreateShader(GL_FRAGMENT_SHADER);
	glShaderSource(fsH,1,&fsrc1,NULL);
	glCompileShader(fsH);
	shaderCheck(fsH);
	glE();
	//Link
	programHandle = glCreateProgram();
	glBindAttribLocation(programHandle,0,"coord2d");
	glBindAttribLocation(programHandle,1,"texCoord");
	glBindAttribLocation(programHandle,2,"texc");
	glE();
	glAttachShader( programHandle, vsH );
	glAttachShader( programHandle, fsH );
	glE();
	glLinkProgram( programHandle );
	programCheck(programHandle);
	glUseProgram( programHandle );
	glE();
	//Pack static data into a VAO
	float vecData[] = {
		-1, 1,
		-1,-1,
		1, 1,
		-1,-1,
		1,-1,
		1, 1
	};
	float texData[] = { 
		0,1,
		0,0,
		1,1,
		0,0,
		1,0,
		1,1
	};
	vboHandles=(GLuint*)malloc(sizeof(int)*2);
	glGenBuffers(2,vboHandles);
	GLuint vecBuf=vboHandles[0];
	GLuint texBuf=vboHandles[1];
	glBindBuffer(GL_ARRAY_BUFFER, vecBuf);
	glBufferData(GL_ARRAY_BUFFER, 8*2 * sizeof(GLfloat), vecData, GL_STATIC_DRAW);
	glBindBuffer(GL_ARRAY_BUFFER, texBuf);
	glBufferData(GL_ARRAY_BUFFER, 8*2 * sizeof(GLfloat), texData, GL_STATIC_DRAW);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glE();
	glGenVertexArrays( 1, &vaoHandle );
	glBindVertexArray(vaoHandle);
	glEnableVertexAttribArray(0);  // Vertex position
	glEnableVertexAttribArray(1);  // Vertex color
	glBindBuffer(GL_ARRAY_BUFFER, vecBuf);
	glVertexAttribPointer( 0, 2, GL_FLOAT, GL_FALSE, 0, (GLubyte *)NULL );
	glBindBuffer(GL_ARRAY_BUFFER, texBuf);
	glVertexAttribPointer( 1, 2, GL_FLOAT, GL_FALSE, 0, (GLubyte *)NULL ); //2 attribute per vertex
	glBindBuffer(GL_ARRAY_BUFFER,0);
	glBindVertexArray(0);
	glE();
}
void qt5nativehandle::initTextures()
{
	g_windowWidth=100;
	g_windowHeight=100;
	//
	glActiveTexture(GL_TEXTURE0);
	glGenTextures(1,&texture);
	glBindTexture(GL_TEXTURE_2D,texture);
	auto hack = (unsigned char*) malloc(sizeof(unsigned char)*g_windowWidth*g_windowHeight);// forogot how to do a nullcopy
	glTexImage2D(GL_TEXTURE_2D,0,GL_R8,g_windowWidth,g_windowHeight,0,GL_RED,GL_UNSIGNED_BYTE,hack);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	GLint swizzleMask[] = {GL_RED, GL_RED, GL_RED, GL_ZERO};
	glTexParameteriv(GL_TEXTURE_2D, GL_TEXTURE_SWIZZLE_RGBA, swizzleMask);
	glBindTexture(GL_TEXTURE_2D,0);
	free(hack);
}
qt5nativehandle::~qt5nativehandle()
{
	done=true;
	t->join();
}


