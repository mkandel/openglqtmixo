/********************************************************************************
** Form generated from reading UI file 'qt5nativehandle.ui'
**
** Created by: Qt User Interface Compiler version 5.2.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_QT5NATIVEHANDLE_H
#define UI_QT5NATIVEHANDLE_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_qt5nativehandleClass
{
public:
    QWidget *centralWidget;
    QVBoxLayout *verticalLayout;
    QGridLayout *gridLayout;
    QPushButton *btnQuit;
    QWidget *wgdMessWith;

    void setupUi(QMainWindow *qt5nativehandleClass)
    {
        if (qt5nativehandleClass->objectName().isEmpty())
            qt5nativehandleClass->setObjectName(QStringLiteral("qt5nativehandleClass"));
        qt5nativehandleClass->resize(600, 400);
        centralWidget = new QWidget(qt5nativehandleClass);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        verticalLayout = new QVBoxLayout(centralWidget);
        verticalLayout->setSpacing(6);
        verticalLayout->setContentsMargins(11, 11, 11, 11);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        gridLayout = new QGridLayout();
        gridLayout->setSpacing(6);
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        btnQuit = new QPushButton(centralWidget);
        btnQuit->setObjectName(QStringLiteral("btnQuit"));

        gridLayout->addWidget(btnQuit, 1, 0, 1, 1);

        wgdMessWith = new QWidget(centralWidget);
        wgdMessWith->setObjectName(QStringLiteral("wgdMessWith"));
        wgdMessWith->setStyleSheet(QStringLiteral("border: 1px solid red"));

        gridLayout->addWidget(wgdMessWith, 0, 0, 1, 1);


        verticalLayout->addLayout(gridLayout);

        qt5nativehandleClass->setCentralWidget(centralWidget);

        retranslateUi(qt5nativehandleClass);

        QMetaObject::connectSlotsByName(qt5nativehandleClass);
    } // setupUi

    void retranslateUi(QMainWindow *qt5nativehandleClass)
    {
        qt5nativehandleClass->setWindowTitle(QApplication::translate("qt5nativehandleClass", "qt5nativehandle", 0));
        btnQuit->setText(QApplication::translate("qt5nativehandleClass", "Quit", 0));
    } // retranslateUi

};

namespace Ui {
    class qt5nativehandleClass: public Ui_qt5nativehandleClass {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_QT5NATIVEHANDLE_H
