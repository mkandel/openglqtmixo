#include "dviSLMDevice.h"
#include "timerpriority.h"
#define glE() _glE(__FILE__,__LINE__)
void _glE(const char *file, int line)
{
#ifndef brave
	GLenum err=glGetError();
	if (!err)
	{
		return;
	}
	const char*  errString =(const char*)gluErrorString(err);
	cout <<"Glu Error:"<<file <<":"<<line <<": " << errString << endl;
	exit(20);
#endif
}

#define shaderCheck(sh) _shaderCheck(sh,__FILE__,__LINE__)
void _shaderCheck(GLint sh,const char *file, int line)
{
	GLint status;
	glGetShaderiv( sh, GL_COMPILE_STATUS, &status );
	if (GL_FALSE == status)
	{
		GLint logLen;
		glGetShaderiv( sh, GL_INFO_LOG_LENGTH, &logLen );
		if( logLen > 0 )

		{
			char * log = (char *)malloc(logLen);
			GLsizei written=0;
			glGetShaderInfoLog(sh, logLen, &written, log);
			cout <<"SHDR Error:"<<file <<":"<<line <<": " <<log << endl;
			free(log);
		}
		exit(20);
	}
}

#define programCheck(ph) _programCheck(ph,__FILE__,__LINE__)
void _programCheck(GLint ph,const char *file, int line)
{
	GLint status;
	glGetProgramiv( ph, GL_LINK_STATUS, &status );
	if (GL_FALSE == status)
	{
		GLint logLen;
		glGetProgramiv( ph, GL_INFO_LOG_LENGTH, &logLen );
		if( logLen > 0 )
		{
			char * log = (char *)malloc(logLen);
			GLsizei written=0;
			glGetProgramInfoLog(ph, logLen, &written, log);
			cout <<"PRGM Error:"<<file <<":"<<line <<": " <<log << endl;
			free(log);
		}
		exit(20);
	}
}

dviSlmDevice* that = 0;
int oneWeWant = 0;

void dviSlmDevice::setFrameToMonochrome(int frame,unsigned char value)
{
	int bytes = sizeof(GLubyte)*g_windowWidth*g_windowHeight;
	assert(bytes >0);
	if (frame >= textureData.size())
	{
		auto p = (GLubyte*) malloc(bytes);
		textureData.push_back(p);
	}
	memset(textureData[frame],value,bytes);
}
void dviSlmDevice::load8bittiff(int frame, const unsigned char* data, int bytes)
{
	int b = sizeof(GLubyte)*g_windowWidth*g_windowHeight;
	//cout << b <<":"<<bytes << endl;
	assert(b==bytes);
	if (frame >= textureData.size())
	{
		auto p = (GLubyte*) malloc(bytes);
		textureData.push_back(p);
	}
	memcpy(textureData[frame],data,bytes);
}
dviSlmDevice::dviSlmDevice(int monitorIn) : monitor(monitorIn)
{
	oneWeWant=monitorIn;
	that = this;
	cv = new condition_variable();
	hasValidGlContext= false;
	m = new mutex();
	//Window Thread
	sb = new spinning_barrier(2);
	windowThread = new thread(&dviSlmDevice::Init,this);	
	sb->wait();
	delete sb;
	//Could happen from windowThread in a updated version
	sb = new spinning_barrier(2);
	openglThread = new thread(&dviSlmDevice::glLoop,this);	
	sb->wait();
	delete sb;
}

dviSlmDevice::~dviSlmDevice(void)
{
	close();
	openglThread->join();
	delete openglThread;
	PostThreadMessage(m_idThread,WM_QUIT,0,0);//PostQuitMessage(0);
	windowThread->join();
	delete windowThread;
	int s =  textureData.size();
	for (int i = 0 ;i <s;i++)
	{
		free(textureData[i]);
	}
	textureData.clear();
	//FACT: You can't copy this class or else the data will be destructed twice!
}

void dviSlmDevice::close()
{
	done = true;
	setFrame(0);//dirty hack for tear down, why the hell did I write this ~MK
}

LRESULT CALLBACK dviSlmDevice::WindowProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
	switch (msg)
	{
	case WM_CHAR:
		switch (static_cast<int>(wParam))
		{
		case VK_ESCAPE:
			PostMessage(hWnd, WM_CLOSE, 0, 0);
			break;

		default:
			break;
		}
		break;

	case WM_DESTROY:
		PostQuitMessage(0);
		return 0;
		/*
		case WM_SIZE://shouldn't happen
		that->g_windowWidth = static_cast<int>(LOWORD(lParam));
		that->g_windowHeight = static_cast<int>(HIWORD(lParam));
		break;
		*/

	case WM_CREATE:
		//that->launchglLoop();
		//cout <<"Created " << endl;
		break;

	default:
		break;
	}

	return DefWindowProc(hWnd, msg, wParam, lParam);
}

void dviSlmDevice::initTextures()
{
	//
	glActiveTexture(GL_TEXTURE0);
	glGenTextures(1,&texture);
	glBindTexture(GL_TEXTURE_2D,texture);
	auto hack = (unsigned char*) malloc(sizeof(unsigned char)*g_windowWidth*g_windowHeight);// forogot how to do a nullcopy
	glTexImage2D(GL_TEXTURE_2D,0,GL_R8,g_windowWidth,g_windowHeight,0,GL_RED,GL_UNSIGNED_BYTE,hack);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	GLint swizzleMask[] = {GL_RED, GL_RED, GL_RED, GL_ZERO};
	glTexParameteriv(GL_TEXTURE_2D, GL_TEXTURE_SWIZZLE_RGBA, swizzleMask);
	glBindTexture(GL_TEXTURE_2D,0);
	free(hack);
}

void dviSlmDevice::initShaders()
{
	//Reserve the textures and make the shaders
	const char *vsrc1 =
		"attribute vec2 coord2d;\n"
		"attribute vec4 texCoord;\n"
		"varying vec4 texc;\n"
		"void main()\n"
		"{\n"
		"vec4 vPosition =vec4(coord2d, 0.0, 1.0);\n"
		"gl_Position = vPosition; \n"
		"texc = texCoord;\n"
		"}\n";
	GLint vsH = glCreateShader(GL_VERTEX_SHADER);
	glShaderSource(vsH,1,&vsrc1,NULL);
	glCompileShader(vsH);
	shaderCheck(vsH);
	glE();
	const char *fsrc1 =
		"uniform sampler2D texture;\n"
		"varying vec4 texc;\n"
		"void main(void)\n"
		"{\n"
		"    gl_FragColor = texture2D(texture, texc.st);\n"
		"}\n";
	GLint fsH = glCreateShader(GL_FRAGMENT_SHADER);
	glShaderSource(fsH,1,&fsrc1,NULL);
	glCompileShader(fsH);
	shaderCheck(fsH);
	glE();
	//Link
	programHandle = glCreateProgram();
	glBindAttribLocation(programHandle,0,"coord2d");
	glBindAttribLocation(programHandle,1,"texCoord");
	glBindAttribLocation(programHandle,2,"texc");
	glE();
	glAttachShader( programHandle, vsH );
	glAttachShader( programHandle, fsH );
	glE();
	glLinkProgram( programHandle );
	programCheck(programHandle);
	glUseProgram( programHandle );
	glE();
	//Pack static data into a VAO
	float vecData[] = {
		-1, 1,
		-1,-1,
		1, 1,
		-1,-1,
		1,-1,
		1, 1
	};
	float texData[] = { 
		0,1,
		0,0,
		1,1,
		0,0,
		1,0,
		1,1
	};
	vboHandles=(GLuint*)malloc(sizeof(int)*2);
	glGenBuffers(2,vboHandles);
	GLuint vecBuf=vboHandles[0];
	GLuint texBuf=vboHandles[1];
	glBindBuffer(GL_ARRAY_BUFFER, vecBuf);
	glBufferData(GL_ARRAY_BUFFER, 8*2 * sizeof(GLfloat), vecData, GL_STATIC_DRAW);
	glBindBuffer(GL_ARRAY_BUFFER, texBuf);
	glBufferData(GL_ARRAY_BUFFER, 8*2 * sizeof(GLfloat), texData, GL_STATIC_DRAW);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glE();
	glGenVertexArrays( 1, &vaoHandle );
	glBindVertexArray(vaoHandle);
	glEnableVertexAttribArray(0);  // Vertex position
	glEnableVertexAttribArray(1);  // Vertex color
	glBindBuffer(GL_ARRAY_BUFFER, vecBuf);
	glVertexAttribPointer( 0, 2, GL_FLOAT, GL_FALSE, 0, (GLubyte *)NULL );
	glBindBuffer(GL_ARRAY_BUFFER, texBuf);
	glVertexAttribPointer( 1, 2, GL_FLOAT, GL_FALSE, 0, (GLubyte *)NULL ); //2 attribute per vertex
	glBindBuffer(GL_ARRAY_BUFFER,0);
	glBindVertexArray(0);
	glE();
}

BOOL CALLBACK MonitorEnumProc(HMONITOR hMonitor, HDC hdcMonitor, LPRECT lprcMonitor, LPARAM dwData)
{

	static int num = 0;
	if ((num++)==oneWeWant)
	{
		auto monitorInfo = (MONITORINFO*) dwData;
		monitorInfo->cbSize = sizeof(MONITORINFO);
		GetMonitorInfo(hMonitor,monitorInfo);
	}
	return TRUE;
}
void dviSlmDevice::createWindow()
{
	//Application Window
	auto hInstance = GetModuleHandle(NULL);
	wcl.cbSize = sizeof(wcl);
	wcl.style = CS_OWNDC | CS_HREDRAW | CS_VREDRAW;
	wcl.lpfnWndProc = WindowProc;
	wcl.cbClsExtra = 0;
	wcl.cbWndExtra = 0;
	wcl.hInstance = g_hInstance = hInstance;
	wcl.hIcon = LoadIcon(0, IDI_APPLICATION);
	wcl.hCursor = LoadCursor(0, IDC_ARROW);
	wcl.hbrBackground = 0;
	wcl.lpszMenuName = 0;
	wcl.lpszClassName = L"SLM_window";
	wcl.hIconSm = 0;
	RegisterClassEx(&wcl);
	DWORD wndExStyle = 0;
	DWORD wndStyle =  WS_POPUP | WS_SYSMENU;
	g_hWnd = CreateWindowEx(wndExStyle,wcl.lpszClassName, L"SLM", 
		wndStyle, 0, 0, 0, 0, 0, 0, wcl.hInstance, 0);
	assert(g_hWnd);
	//Move to monitor
	EnumDisplayMonitors(NULL, NULL, MonitorEnumProc,(LPARAM)&monitorInfo);
	RECT rc = monitorInfo.rcMonitor;
	MoveWindow(g_hWnd, rc.left, rc.top, rc.right - rc.left, rc.bottom - rc.top, TRUE);
	GetClientRect(g_hWnd, &rc);
	g_windowWidth = rc.right - rc.left;
	g_windowHeight = rc.bottom - rc.top;
}

int dviSlmDevice::Init()
{
	createWindow();
	m_idThread = GetCurrentThreadId();
	sb->wait();
	windowLoop();
	g_hDC = 0;
	UnregisterClass(wcl.lpszClassName,wcl.hInstance);
	return 0;
}

void dviSlmDevice::setFrame(int framenum)
{
	//Sets the frame and locks until the frame has actually changed, this is how we established synchronization 
	//Also the correct option needs to be set in the NVidia control panel specifical, vysnc should be enabled , if not forced
	assert(frame < textureData.size());
	frame = framenum;
	{
		std::lock_guard<std::mutex> lk(m[0]);
		ping = true;
	}
	cv->notify_one();
	{
		std::unique_lock<std::mutex> lk(m[0]);
		cv->wait(lk, [&]{return pong;});
	}
	pong = false;
}

void dviSlmDevice::renderFrame()
{
#if 1
	static int tic = -1;
	tic = ++tic %2;
	glClearColor(tic, tic, tic, 0.0f);
#endif
	//glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);
	//
	glClear(GL_COLOR_BUFFER_BIT);
	glBindTexture(GL_TEXTURE_2D,texture);
	glTexSubImage2D(GL_TEXTURE_2D,0,0,0,g_windowWidth,g_windowHeight,GL_RED,GL_UNSIGNED_BYTE,textureData[frame]);
	glBindVertexArray(vaoHandle);
	glDrawArrays(GL_TRIANGLES,0,8);
	glBindVertexArray(0);
	glBindTexture(GL_TEXTURE_2D,0);
	glFlush();
	glFinish();
	glE();
}

int dviSlmDevice::windowLoop()
{
	MSG msg;
	// loop until WM_QUIT(0) received
	while(GetMessage(&msg, NULL, 0, 0) > 0)
	{
		TranslateMessage(&msg);
		DispatchMessage(&msg);
	}
	return (int)msg.wParam;
}

void dviSlmDevice::InitGL()
{
	//
	g_hDC = GetDC(g_hWnd);
	int pf = 0;
	// Create and set a pixel format for the window.
	PIXELFORMATDESCRIPTOR pfd;
	memset(&pfd,0,sizeof(PIXELFORMATDESCRIPTOR));
	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pfd.iPixelType = PFD_TYPE_RGBA;
	pfd.cColorBits = 16;
	pfd.cDepthBits = 8;
	pfd.iLayerType = PFD_MAIN_PLANE;

	OSVERSIONINFO osvi = {0};
	osvi.dwOSVersionInfoSize = sizeof(OSVERSIONINFO);
	GetVersionEx(&osvi);

	pf = ChoosePixelFormat(g_hDC, &pfd);
	SetPixelFormat(g_hDC, pf, &pfd);
	//Too lazy to find this code, so I grabbed from somewhere ~MK
	// When running under Windows Vista or later support desktop composition.
	// This doesn't really apply when running in full screen mode.
	if (osvi.dwMajorVersion > 6 || (osvi.dwMajorVersion == 6 && osvi.dwMinorVersion >= 0))
	{
		pfd.dwFlags |= PFD_SUPPORT_COMPOSITION;
	}
	// Creates an OpenGL 3.1 forward compatible rendering context.
	// A forward compatible rendering context will not support any OpenGL 3.0
	// functionality that has been marked as deprecated.
	//Bootstrap glew
	HGLRC tempContext = wglCreateContext(g_hDC);
	wglMakeCurrent(g_hDC, tempContext);
	auto foo = glewInit();
	if (foo != GLEW_OK)
	{
		cout <<"Oh NO!" << endl;
	}
	int attribList[] =
	{
		WGL_CONTEXT_MAJOR_VERSION_ARB, 3,
		WGL_CONTEXT_MINOR_VERSION_ARB, 1,
		WGL_CONTEXT_FLAGS_ARB, WGL_CONTEXT_FORWARD_COMPATIBLE_BIT_ARB,
		0, 0
	};
	//
	g_hRC = wglCreateContextAttribsARB(g_hDC,0, attribList);
	wglMakeCurrent(NULL,NULL);
	wglDeleteContext(tempContext);
	wglMakeCurrent(g_hDC, g_hRC);
	SetWindowText(g_hWnd, L"SLM");
	//initShaders();
}

int dviSlmDevice::glLoop()
{
	setFrameToMonochrome(0,0);//some default data to avoid null refernces, etc
	frame = 0;
	done = false; 
	//
	InitGL();
	glE();
	//
	wglSwapIntervalEXT(1);
	initShaders();
	initTextures();
	ShowWindow(g_hWnd,SW_MAXIMIZE);
	sb->wait();
	while (!done)
	{
		{
			std::unique_lock<std::mutex> lk(m[0]);
			cv->wait(lk, [&]{return ping;});
		}
		ping = false;
		//Display Frame
		renderFrame();
		SwapBuffers(g_hDC); // on gdi
		{
			std::lock_guard<std::mutex> lk(m[0]);
			pong = true;
		}
		cv->notify_one();
	}
	wglDeleteContext(g_hRC);
	g_hRC = 0;
	ReleaseDC(g_hWnd, g_hDC);
	return 0;
}